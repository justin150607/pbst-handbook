module.exports = [{
    text: 'Koti',
    link: '/fi/'
},
{
    text: 'PBST-käsikirja',
    link: '/fi/pbst/'
},
{
    text: 'Kiitokset',
    link: '/fi/credits/'
}
,
    {
        text: 'Pinewood',
        items: [{
                text: 'PET-käsikirja',
                link: 'https://pet.pinewood-builders.com'
            },
            {
                text: 'TMS-käsikirja',
                link: 'https://tms.pinewood-builders.com'
            }
        ]
    }
]
