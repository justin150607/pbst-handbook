---
home: true
heroImage: /PBST-Logo.png
actionText: Tehdään tämä →
actionLink: pbst/
---
::: warning HUOMIO:
 - `Eurooppalainen päivämäärä: 9.3.2020`
 - `Amerikkalainen päivämäärä: 3.9.2020`

lähtien tämä käsikirja on PBST:n virallinen käsikirja. Kuten Csdi sanoi kyseisenä päivänä: ![img](https://i.imgur.com/ZVyPdZb.png)

Alkuperäinen käsikirja löytyy [täältä](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook-11-10-2019/385368). Tue myös alkuperäistä käsikirjaa :smile: 
:::

::: danger VAROITUS! 
Lue kaikki säännöt erittäin tarkasti. Kaikista sääntöjen muutoksista tiedotetaan selkeästi. Säännöt on ryhmitelty sen mukaan, ketä ne koskevat, mutta osa säännöistä koskee useampaa PBST-jäsenten osajoukkoa. Tässä tapauksessa asiasta kerrotaan selkeästi. Sääntöjen rikkomisesta seuraavat rangaistukset riippuvat säännöstä ja rikkomuksen vakavuudesta. Rangaistukset vaihtelevat varoituksesta arvonalennukseen tai jopa porttikieltoon PBST:stä. 
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>