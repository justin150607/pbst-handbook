# Tier 4 -jäsenten ja kouluttajien komennot

:::warning Näitä komentoja käytetään yleensä koulutuksissa. 
Suurta osaa komennoista tällä sivulla käyttävät avustajat, Tier 4 -jäsenet ja kouluttajat.   
Jokaisessa PB-laitoksessa on lupajärjestelmä, joten nämä komennot alle Tier 4 -arvoisena jäsenenä eivät toimi.

Koulutusta järjestäessä tai sellaisessa avustaessa täytyy muistaa, että koulutus annetaan **ENGLANNIKSI**. Varmista siis että puhut englantia sujuvasti tai vähintään ymmärrettävästi. 
:::

:::danger Älä vain kopioi ja liitä Huomaa, että nämä komennot ovat **ESIMERKKEJÄ**. 
Sinun täytyy muuttaa niitä sopivaksi koulutukseen tai yrittämääsi asiaan.  
Varmista, että olet kokeillut komentoja aiemmin ennen niiden käyttöä **OIKEASSA** koulutuksessa... 
:::

## Mahdolliset argumentit
- `me` –> Sinä itse.
- `pelaajan-nimi` –> Valitsemasi pelaaja.
- `all` –> Kaikki serverissä olevat (Myös sinä itse ja tarkkailijat).
- `%joukkueen-nimi` –> Valitsee joukkueen (**Esimerkki**: `;add %Winners Win 1`).
- `nonadmins` –> Jokainen käyttäjä, jolla ei ole lupaa käyttää ylläpitokomentoja (Todennäköisimmin kaikki alle Tier 4 -arvoiset. **HUOMIO**: Tämä argumentti saattaa valita myös tarkkailijat).
- `radius-` –> Kaikki antamallasi etäisyydellä. (**Esimerkki**: `;team radius-20 winners`)
- `others` –> Tämä valitsee kaikki paitsi sinut itse (Jos sinulla on siis avustajia tai tarkkailijoita, tämä valitsee myös ne)

## Mitä komentoja käytetään yleensä koulutuksissa?
- `;fly | ;god`  
  Tämä sarja komentoja antaa sinun lentää ja tekee sinusta vahingoittumattoman
- `;team <argument> <team>`  
  Siirrä pelaaja joukkueeseen riippuen siitä, mitä joukkueita olet luonut alla olevilla komennoilla
- `;newteam Host/Assistants <color>`  
  Kun johdat koulutusta, tämä komento luo joukkueen nimeltä "Host/Assistants".
- `;newteam Observers <color>`  
  Luo joukkue tarkkailijoille (Koulutusta katsoville pelaajille. **HUOMIO**: Tarkkailijoille ei anneta pisteitä)
- `;newteam Winners <color>`  
  Joukkue, jonka avulla määritellään aktiviteetin tai pelin voittajat ja annetaan heille voittopisteitä (Näin et anna vahingossa kaikille pisteitä)
- `;newteam Security <color>`  
  Tämä on oletusjoukkue, jossa kaikkien tulisi olla (Lukuun ottamatta koulutuksen järjestäjää, tarkkailijoita ja avustajia) ![img](https://i.imgur.com/vLwSyFK.png)

- `;addstat Wins`  
  Luo luvun osallistujien voittopisteiden seuraamiseen
- `;m <message>`  
  Näyttää kaikkien näytöllä tiedotteen ![img](https://i.imgur.com/PcjEr9v.png)
- `;n <message>`  
  Pieni viesti, joka näkyy kaikille ![img](https://i.imgur.com/rZKvc03.png)
- `;h <message>` ![img](https://i.imgur.com/YXRuuJB.png) Näyttää viestin kaikille näytön yläreunassa
- `;add <argument> Wins 1`  
  Antaa jollekin voittopisteen (Pelaajien kanssa ei tarvitse käyttää %-merkkiä, mutta joukkueita valitessa tarvitsee)
- `;team radius-<distance> <Team>`  
  Asettaa ympärilläsi olevat pelaajat joukkueeseen. Tämä on erittäin hyödyllinen voittopisteitä jakaessa.
- `;give <argument> <tool>`  
  Antaa pelaajille tietyn työkalun
- `;sword <argument>`  
  Lyhyt komento, jolla voidaan antaa miekka pelaajille
- `;removetools <argument>`  
  Poistaa valituilta pelaajilta kaikki työkalut ja tavarat
- `;viewtools <argument>`  
  Näyttää sinulle, mitä työkaluja pelaajalla on
- `!importalias <!alias> <Command>`  
  Käytä tätä komentoa lisätäksesi aliaksen omaan kokoelmaasi (Esimerkki: **!addalias !shirtme !shirt [146487695](https://www.roblox.com/catalog/146487695/Security-Vest-Specops-PB)**)

## Aliasehdotuksia

 1. Avaa Kronos klikkaamalla näytön oikeaa alakulmaa (PBST logo)
 2. Avaa "Aliases"-välilehti
 3. Paina "Add"-nappia ikkunan alaosassa
 4. Lisää alias, jota haluat käyttää komentona kenttään **Alias:**
 5. Syötä jokin alla olevista komennoista tai oma komentosi

**`;assist`**:
- `;team <arg1> Host/Assistants | ;fly <arg1> | ;god <arg1> | ;sword <arg1>` Antaa henkilölle avustajan komennot ja siirtää hänet Host/Assistants-joukkueeseen. Kouluttajat voivat käyttää ylimääräisiä komentoja parantaakseen aliasta:
- `;team <arg1> Host/Assistants | ;admin <arg1> | ;loadout <arg1> Special` Antaa henkilölle väliaikaiset moderaattorikomennot koulutuksen ajaksi ja antaa pelaajalle Tier 4 (Special Defence) aseet.

**`;observe`**:
- `;team <arg1> Obs | ;fly <arg1> | ;god <arg1>`   
  Siirtää pelaajan tarkkailijoille varattuun joukkueeseen, antaa hänen lentää ja tekee hänestä vahingoittumattoman  
  ![img](https://i.imgur.com/imuV43Y.png)

**`;s2vote`**:
- `;vote nonadmins,-%ob Sword_Fight,Guns,Juggernaut,Bombs 20 What activity shall we do?` Hyödyllinen koulutuksen järjestäjälle valitessa, mitä areenaa pitäisi käyttää sektorilla 2.

**`;noreturn`**:
- `;lobbydoors off | ;teleporters off`  
  Lukitsee aulan, milloin kukaan ei voi lähteä sieltä

**`;bafk`**:
- `;bring <arg1> | ;afk <arg1>` Tuo pelaajan luoksesi ja AFK:aa hänet.

**`;flygod`**:
- `;fly <arg1> | ;god <arg1>` Tämä antaa valitun pelaajan lentää ja tekee tästä vahingoittumattoman.

**`;bjail`**:
- `;bring <arg1> | ;jail <arg1>` Tuo pelaajan luoksesi ja vangitsee hänet eteesi.

**`;win20`**:
- `;team radius-20 %Winners`

**`;twin`**:
- `;add %Winners Wins 1`

**`;unteamwin`**:
- `;unteam %Winners` Tätä prosessia käytetään voittopisteiden antamisessa tietyille pelaajille helposti

**`;trainingsetup`**:
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on`
- `;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on | ;addstat Strikes` (Tätä voi käyttää myös **`;strike`** -aliaksen kanssa) Luo jokaisen tarvitsemasi joukkueen sekä "Wins" tilaston. Tehdäksesi tästä aliaksesta kehittyneemmän, vaihtoehtoja on lueteltu tässä kohdassa useita.


**Kiitokset käyttäjille *MarusiFyren* ja *EquilibriumCurse* avusta tätä sivua luodessa ja kiitokset Marusille joistakin tällä sivulla käytetyistä kuvista.**