
# PBST-käsikirja

Seuraava verkkosivu on virallinen PBST-käsikirja, jonka ovat kirjoittaneet PBST:n kouluttajat kaikille PBST:n jäsenille (sivun on luonut superstefano4/Stefano). Lue käsikirja huolellisesti. Käsikirjan mukaan toimimatta oleminen saattaa johtaa rangaistuksiin, jotka vaihtelevat varoituksesta arvonalennukseen tai jopa porttikieltoon ryhmästä. Kaikista käsikirjaan tehdyistä muutoksista tiedotetaan selkeästi.

# Muistutuksia kaikille PBST:n jäsenille
  * Noudata aina [ROBLOXin yhteisösääntöjä](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules).
  * Kunnioita muita pelaajia, äläkä käyttäydy kuten omistaisit pelin.
  * Noudata korkeammalle sijoittuvien jäsenien käskyjä.
  * Emme ole sodassa Innovation Inc. -ryhmän kanssa, joten älä ole vihamielinen heitä kohtaan millään tavalla.
  * Älä yritä kierrellä rangaistuksia millään tavalla. Tämän tekeminen johtaa ainoastaan ankarampien rangaistuksien saamiseen.
  * Palveluksessa tai ei, pelaajien satunnaisesti tappaminen suurissa määrin ja spawn-alueella tappaminen on aina kielletty. Käytä !call-komentoa kutsuaksesi PIA:n apuusi mikäli joku toimii näin.
  * Kouluttajat ovat vapautettuja suurimmasta osasta käsikirjan säännöistä, lukuun ottamatta edellä mainittuja muistutuksia.

## Palvelukseen astuminen partiointia varten
Ennen kuin voit tehdä työsi Pinewood-vartijana, täytyy sinun näyttää siviileille ja muille PBST-jäsennille olevasi palveluksessa vartijana. Tehdäksesi tämän sinun täytyy käyttää univormua ja asettaa Kronos-arvomerkkisi asetukseen "Security". Pelkästään toinen näistä ei yksin riitä laskemaan sinua palveluksessa olevaksi, jolloin PBST-aseiden ottaminen on kielletty.

### Univormu
Viralliset PBST-univormut löytyvät [kaupasta](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store). Kaikissa laitoksissa näytölläsi on ostoskärrynappi, jonka kautta voit myös ostaa näitä univormuja. Univormun ostaminen ja päälle laittaminen Roblox-hahmollesi on kaikista kätevin tapa käyttää virallista univormua.

Jos sinulla ei ole robuxeja, suurimmassa osassa laitoksista löytyy paikka, josta univormun saa käyttöönsä väliaikaisesti ilmaiseksi. On myös mahdollista ostaa lahjoittajan komennot ja käyttää niiden mukana tulevia !shirt ja !pants -komentoja pitääksesi ylläsi univormua.

#### Univormujen sijainnit
* [Pinewood Computer Core:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core)  
  Pääspawn-alueelta aulan läpi, hissillä ylös (seuraa "Security Sector + Cafe" -opastetta), PBST-huoneessa ovesta oikealle
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility)  
  Pääspawn-alueelta huoneeseen "PB SEC", käytävään pampun ja kahviautomaatin välissä, pukuhuone vasemmalla
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ)  
  4. kerros, hisseistä vasemmalla olevien lasiovien kautta, siniset ovet vasemmalla
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility)  
  Spawn-rakennuksen oikeassa reunassa
* [PBST Activity Center:](https://www.roblox.com/games/1564828419/PBST-Activity-Center)  
  Aulan pääkäytävässä

PET-palokunnan tai hazmat-osaston univormun käyttäminen on sallittu, mutta vain jos virallista PBST-univormua käytetään sen kanssa. Toisin kuin edellä mainitut puvut, lääkintäosaston puvun käyttäminen ei ole sallittua, sillä tämän käyttäminen muuttaa hahmosi paidan ja housut, joka tekee PBST-univormun käyttämisestä samaan aikaan mahdotonta.

### Arvomerkki
Arvomerkki on pieni Pinewood-laitoksissa hahmosi yläpuolella näkyvä teksti. Tämä on oletuksena "Security" -asetuksella. Jos se on pois päältä, voit käyttää komentoa `!ranktag on` laittaaksesi arvomerkin päälle. Jos se on asetettuna johonkin muuhun ryhmään kuin Security, käytä komentoa `!setgroup PBST` asettaaksesi sen "Security" -tilaan, jotta voit partioida.

Virantoimituksessa olevana PBST-jäsenenä sinun on määrä suojella laitosta erilaisilta ilmeentyviltä uhilta, kuten ytimen sulamiselta, hyökkääjiltä ja OP-aseiden käyttäjiltä. Kun sinun tarvitsee evakuoida, pyri parhaasi mukaan auttamaan muita.

### Virantoimituksen lopettaminen
Lopettaaksesi virantoimituksen, sinun täytyy käydä vartijahuoneessa ja poistaa univormusi ja PBST-aseesi. Resetoi hahmosi tarvittaessa. On erittäin suositeltavaa, että poistat myös PBST-arvomerkkisi käytöstä komennolla `!setgroup PB/PET/PBQA...` vaihtaaksesi sen mihin tahansa muuhun Pinewood-ryhmään, johon kuulut, tai käyttämällä komentoa !ranktag off piilottaaksesi arvomerkin kokonaan.

### Aseiden käyttö
PBST-jäsenten aseet löytyvät univormujen lähistöltä. Muita jakelulaitteita saattaa löytyä eri puolilta laitosta. Jokaisella PBST-jäsenellä on oikeus tavalliseen pamppuun ja Tier 1+ -jäsenet saavat ylimääräisiä aseita sinisistä jakelulaitteista. **Älä anna aseita kenellekään alemmas sijoittuvalle**. Tämä koskee myös joistakin laitoksista löytyviä ohjattavia tappavien aseiden jakelulaitteita. Ainoa poikkeus tähän on kun Tier 4+ -jäsen pyytää sinua tekemään näin.

Näitä PBST-aseita **ei saa** väärinkäyttää millään tavalla (Pelaajien tappaminen satunnaisesti ilman syytä, spawn-alueella tappaminen jne.). Cadet-arvoisia PBST-aseiden väärinkäyttäjiä varoitetaan (Lue [Sääntöjen rikkojat](/#how-to-deal-with-rulebreakers) -osio). Jos huomaat Tier 1+ -jäsenen väärinkäyttävän PBST-aseita, kerää todistusaineistoa ja ilmoita asiasta Tier 4+ -jäsenelle.

Varmista, että sinulla on univormu ottaessasi PBST-aseita, sillä sinut lasketaan vapaalla olevaksi ilman univormua, jolloin et saa käyttää PBST-aseita. PBST-aseita ei saa koskaan käyttää ytimen sulattamiseen tai jäädyttämiseen.

Jos käytät muita kuin PBST-aseita virantoimituksen aikana (Kuten OP-aseita, pisteillä ostettavaa tai satunnaisesti ilmestyvää pistoolia PBCC:ssä), täytyy sinun noudattaa samoja sääntöjä, kuin PBST-aseiden kanssa.

Hätätilanteessa Tier 4 tai korkea-arvoisempi jäsen saattaa antaa sinulle luvan rajoittaa huoneen **ainoastaan PBST:lle**. Tässä tapauksessa voit tappaa alueelle astuvat pelaajat ja päästää vain PBST-jäsenet sisään.

Vapaalla ollessasi et saa käyttää PBST-aseita, mutta sinulla on enemmän  vapauksia aseiden käytön suhteen. Voit esimerkiksi rajoittaa pääsyä huoneisiin ilman erityistä lupaa, mutta liialliset rajoitukset saatetaan laskea suurissa määrissä satunnaisesti tappamiseksi, joka on aina kielletty.

### Miten toimia sääntöjen rikkojien kanssa
Pieniä sääntöjä rikkoville vierailijoille annetaan yleensä varoitus. Sinun täytyy antaa **kaksi** varoitusta pelaajille ennen tappavan voiman käyttöä. Jos vierailija on tapettu kolmen varoituksen jälkeen ja he jatkavat sääntöjen rikkomista, voit tappaa pelaajan ilman, että sinun tarvitsee varoittaa heitä uudelleen.

Jos vierailija yrittää sulattaa tai jäädyttää ytimen muuttamalla reaktorin asetuksia, anna hänelle varoitus. Jos hän reagoi vaarallisesti, voit tappaa pelaajan.

Jos OP-aseita tai muuten hankittuja varusteita käyttävä vierailija hyökkää suoraan vartijan kimppuun, tämä pelaaja voidaan tappaa antamatta varoituksia. Et voi kuitenkaan tappaa kostaaksesi, esimerkiksi vain siksi, että pelaaja tappoi sinut äsken. Voit tappaa pelaajan uudelleen vain, jos hän jatkaa sääntöjen rikkomista (Tässä vaiheessa varoitusten antaminen ei enää ole tarpeen).

Kuka tahansa junalla lentävä pelaaja on määrä tappaa välittömästi, kuka tahansa muulla ajoneuvolla lentävä pelaaja on määrä lamauttaa ja ajoneuvo on määrä poistaa.

Jos näet vartijan rikkovan käsikirjan sääntöjä, kerro hänelle, mitä hän tekee väärin ja anna hänelle varoitus. Jos kyseessä oleva vartija ei kuuntele tai käyttää aseitaan vastuuttomasti, tapa pelaaja.

Jos **Tier 1+ -jäsen** toimii väärin aseidensa kanssa, kutsu PIA hoitamaan tilanne.

### Tappaminen suoralta kädeltä (KoS, Kill on Sight)
Oikeus asettaa pelaajalle KoS-käsky PBST:lle on varattu Tier 4 ja korkeammalle sijoittuville jäsenille. Partioidessa PBST-jäsenet eivät saa antaa KoS-käskyjä, ellei lupaa tähän ole erikseen antanut Tier 4+ -jäsen.

TMS-jäsenet ja mutantit PBCC:ssä on määrä tappaa välittömästi joka tapauksessa.

## Tärkeitä partioitavia laitoksia
### Pinewood Computer Core
Pinewood Computer Core on kaikista tärkein partioitava laitos. Tärkein päämääräsi vartijana täällä on estää ydintä sulamasta tai jäätymästä. Seuraava kaavio näyttää, miten erilaiset asetukset vaikuttavat lämpötilaan: ![img](https://cdn.discordapp.com/attachments/469480731341488138/660483329199439883/unknown.png)

Jos et onnistu suojelemaan ydintä ja ydin sulaa, voit mennä hätäjäähdytysnesteen luokse (Emergency Coolant) sektorilla G yrittääksesi pelastaa ytimen. Käytä turvakoodia päästäksesi sisään (Koodi kuulutetaan pelissä). Pidä hätäjäähdytysneste tasapainossa, kunnes aika loppuu.

Ytimen sulamisen ja jäätymisen lisäksi PBCC:ssä on myös erilaisia "katastrofeja", joista osa vaatii PBST:tä evakuoimaan vierailijoita. Plasmatulvan, maanjäristyksen tai säteilytulvan tapahtuessa vie vaarassa olevat pelaajat turvaan.

Mutantteja esiintyy PBCC:ssä usein. Jos joku astuu vuotavasta radioaktiivisesta aineesta koostuvaan lätäkköön, hän muuttuu mutantiksi. Mutantit on määrä tappaa välittömästi. **PBST-jäsenet eivät saa muuttua mutantiksi virantoimituksen aikana.**

#### TPS-laskin
Tässä käsikirjan versiossa on laskin, jolla voi laskea lämpötilan muutosta sekunnissa (**T**emperature **P**er **S**econd). <Calculator />

### PBST Activity Center (PBSTAC)
PBST Activity Center on tärkein laitos koulutuksille ja muille erilaisille aktiviteeteille. Voit harjoitella muutamia esteratoja, kuten Gamma Obby, Vortex of Rage ja Tall Towers. Sieltä löytyy myös PBCC:n ydintä mukaileva simulaattori kaikkine lämpötilaan vaikuttavine muuttujineen. Ydin voi myös sulaa ja jäätyä.

PBSTAC-laitosta voivat käyttää myös hyökkääjät, jotka tappavat armottomasti OP-aseillaan. Heidät tulee tappaa välittömästi ja laitosta partioien PBST-jäsenten täytyy toimia yhdessä pitääkseen heidät loitolla.

Varo myös hirviöitä, kuten zombeja, luurankoja tai pahempaa…

## Muita ryhmiä joita saatat tavata
### Pinewood Emergency Team
Pinewood Emergency Team (PET) on ryhmä, joka vastaa muutamiin erilaisiin hätätilanteisiin Pinewood-laitoksissa. He auttavat PBST:tä usein työssämme heidän turvassa pitämiseksi. Jos PET-jäseniä ei ole paikalla ja tulipalo tai säteilytulva täytyy hoitaa välittömästi, PBST-jäsenet saavat auttaa tulen sammuttamisessa tai tulvan pysäyttämisessä.

PET:illä on 3 alaosastoa: lääkintä, palokunta ja Hazmat, joista jokaisella on oma univormu. Palokunnan ja Hazmat-ryhmän univormujen kanssa voi pitää PBST-univormua. Lääkintäryhmän univormun kanssa näin ei voi tehdä (Katso [Univormu](/#Uniform)-osio).

Voit vaihtaa virantoimituksesi PBST:lle tai PET:ille käyttämällä !setgroup-komentoa. PET:illä on omat työkalunsa, joita ei saa käyttää vartijana ja päinvastoin.

Lue PET-käsikirja, jos haluat lukea yksityiskohtaisemmin PET:in toiminnasta:
- [PET - DevForum-käsikirja](https://devforum.roblox.com/t/pinewood-emergency-team-handbook/507807)
- [PET - Verkkosivun käsikirja](https://pet.pinewood-builders.com)

### The Mayhem Syndicate
The Mayhem Syndicate (TMS) on PBST:n vastakohta. Siinä missä me pyrimme suojelemaan ydintä, he pyrkivät sulattamaan tai jäädyttämään sen. Voit tunnistaa heidät heidän punaisesta arvomerkistään. He liittyvät peliin usein hyökkäyksen aikana. Tämän tapahtuessa on suositeltavaa kutsua apuvoimia.

TMS-jäsenet tulee tappaa välittömästi (KoS) oletuksena. Ainoa paikka, jossa TMS-jäseniä ei saa tappaa (Spawn-alueen lisäksi) on TMS-varusteiden luona rahtijunien läheisyydessä. Sinun tulee antaa TMS-jäsenille mahdollisuus hakea varusteensa, sillä he tekevät samoin PBST-jäsenille. Älä oleskele TMS-varusteiden luona.

Kuten kaikkien muidenkin aseistettujen hyökkääjien kanssa, saat taistella vastaan jos TMS-jäsen hyökkää kimppuusi, vaikka he olisivatkin matkalla hakemaan varusteitaan.

Jos olet vapaalla TMS-hyökkäyksen alkaessa ja haluat osallistua, sinun täytyy valita puolesi ja taistella kyseisellä puolella hyökkäyksen loppuun asti. **Älä vaihda joukkuetta kesken hyökkäyksen**. Tämä sääntö pätee myös, jos olet neutraali tai vapaalla ja päätät osallistua. Huomioi, että valitsaessa liittyä hyökkäyksessä TMS:än tai PBST:n puolelle saattaa johtaa siihen, että toinen joukkue tappaa sinut välittömästi.

## Koulutukset ja ylennykset
### Koulutukset
::: warning HUOMIO: 
Ohjeet koulutuksissa annetaan englanniksi, joten englannin kielen ymmärtäminen on pakollista. 
:::

Tier 4+ voi järjestää koulutuksia, joissa voit ansaita [pisteitä]((../ranks-and-ranking-up/)). Aikataulu näille koulutuksille löytyy [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility) -laitoksesta.

**Koulutusten säännöt**:
* Kuuntele koulutuksen järjestäjää ja noudata hänen antamia käskyjä.
* Älä ota aseita tai muita varusteita. Järjestäjä antaa sinulle aktiviteetteihin tarvittavat varusteet.
* Pidä aina ylläsi PBST-univormua koulutuksen ajan.
* Kouluttajat saattavat aktivoida puhumislupajärjestelmän (PTS, Permission To Speak), joka tarkoittaa, että luvatta puhuminen johtaa rangaistukseen. Sano "`;pts request`" pyytääksesi lupaa puhua järjestelmän ollessa käytössä.
* Jotkut koulutukset merkitään tiukempina Disciplinary Training -koulutuksina, jotka keskittyvät enemmän kuriin ja käskyjen noudattamiseen. Sinut poistetaan koulutuksesta, jos käyttäydyt huonosti.
* Korkeintaan kerran viikossa saatetaan järjestää Hardcore Training -koulutus. Tämän tyyppisissä koulutuksissa pieninkin virhe johtaa koulutuksesta poistamiseen. Jos "selviydyt" koulutuksen loppuun asti, saatat ansaita jopa 20 pistettä.

Lisää tietoa pisteistä löytyy [täältä](../ranks-and-ranking-up/)

## Arvot
### Tier 1+ -jäsenet
Kun olet suorittanut tasoarvioinnin, sinut ylennetään arvoon Tier 1. Tällä arvolla saat uusia varusteita Pinewood-laitoksissa: Voimakkaamman pampun, kilven, etälamauttimen ja PBST-pistoolin. Nämä aseet löytyvät vartijahuoneista Pinewood-laitoksissa. Cadet-jäsenet eivät saa mennä Tier 1+ -jäsenten välinehuoneisiin. (Vaikka he eivät pysty mennä huoneisiin itse tai käyttää aseiden jakelulaitteita, emme halua tämän tapahtuvan)

Saadaksesi ylennyksen arvoihin Tier 2 ja Tier 3 sinun täytyy vain saada riittävä määrä pisteitä. Saavuttaessasi Tier 2 -arvon, PBST-varusteisiisi lisätään kivääri. Tier 3 -arvoiset jäsenet saavat muiden aseiden lisäksi myös konepistoolin.

Tier-jäsenten odotetaan toimivan roolimalleina kadeteille. Sääntörikkomukset saattavat johtaa tiukempiin rangaistuksiin. Erityisesti Tier 3 -jäsenten odotetaan olevan PBST:n parhaimpia edustajia. **Sinua on varoitettu.**

Tier-jäsenet voivat käyttää `!call PBST` -komentoa, jota voi käyttää lähestyvän ytimen sulamisen tai jäätymisen tai hyökkääjän tapauksessa. **Tätä tulee käyttää viisaasti.**

Tier-jäsenenä sinut saatetaan valita avustamaan koulutuksessa tai arvioinnissa, jolloin sinulle annetaan väliaikaisesti pääsy ylläpitäjien komentoihin, joita tulee käyttää **ainoastaan** kyseistä koulutusta varten. Arvioinnin tai koulutuksen jälkeen menetät nämä komennot. Tämä varmistaa, ettei komentoja käytetä väärin. **Näiden komentojen väärinkäyttöä rangaistaan ankarasti**.

### Tier 4 -jäsenet (Alias Special Defence tai SD)
Tier 3 -jäsenet, jotka ovat saaneet 800 pistettä saatetaan arvioida ylennystä Tier 4 -arvoon varten. Arvioitavan jäsenen täytyy pitää PBST-koulutus ja jäsenen suoritusta valvotaan tarkasti. Jos suoritat arvioinnin, saat arvonimen "`Passed SD Eval`", kunnes kouluttajat päättävät ylentää sinut Tier 4 -arvoon. **Discord-käyttäjätunnus on pakollinen.**

Tier 4 -jäsenenä voit antaa KoS-käskyjä Pinewood-laitoksissa, voit määrätä huoneita rajoitettavaksi ja sinun ei tarvitse enää käyttää univormua. Saat myös Kronos-moderaattoriluvan Pinewoodin koulutuslaitoksissa. **Tätä tulee käyttää vastuullisesti**.

Tier 4 -jäsenet voivat järjestää koulutuksia kouluttajan luvalla. He saavat järjestää koulutuksia 6 kertaa viikossa (Poislukien megakoulutukset) ja korkeintaan 2 kertaa päivässä. Tier 4 -jäsenet eivät saa pyytää lupaa koulutuksen järjestämiseen yli 3 päivää etuajassa. Koulutuksen lopusta seuraavan koulutuksen alkuun täytyy olla vähintään kahden tunnin väli.

### Kouluttajat (Trainer)
Uusi kouluttaja valitaan nykyisten kouluttajien äänestyksellä. Vain Tier 4 -jäsenet saatetaan valita tähän ylennykseen.

Kouluttajat saavat järjestää koulutuksia ilman rajoituksia, mutta kahden tunnin välin sääntö pätee vieläkin. Kouluttajat ovat myös vastuussa PBST:n ylläpitotehtävistä, kuten pisteiden kirjaamisesta ja jäsenten ylentämisestä.

::: warning HUOMIO: 
Nämä säännöt saattavat muuttua milloin tahansa, joten tarkista käsikirja uudelleen ajoittain. 
:::
